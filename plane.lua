local System = require 'knife.system'
local Color = require 'graphics.db16'

local draw = System(
  { 'blocks' },
  function (blocks)
    
    love.graphics.push()

      love.graphics.setColor(Color.DARKRED)

      -- Scale from world-space (1=block) to pixel-space (1=pixel)
      love.graphics.scale(8, 8)

      -- Translate and draw each bone
      for i, block in ipairs(blocks) do
        love.graphics.push()
          love.graphics.rectangle('fill', block[1], block[2], 1, 1)
        love.graphics.pop()
      end
      
    love.graphics.pop()
  end)

return {draw=draw}