local Behavior = require 'knife.behavior'
local System = require 'knife.system'

local function takeNextStep(behavior, entity)
  -- Just finished walking a step, move to that location
  entity.position = table.remove(entity.path)
  
  if next(entity.path) == nil then
    -- No more steps to take
    behavior:setState('idle')
  end
end

local states = {
  default = {
    { duration = 0, after = 'idle' },
  },
  idle = {{ duration = 1 }},
  walk = {
    { duration = 1},
    { duration = 0, action = takeNextStep },
  },
}

local function addAspect(entity)
  entity.behavior = Behavior(states, entity)
end

local update = System(
  { 'behavior', 'path' },
  function (behavior, path, dt)
    if behavior.state == 'idle' and next(path) ~= nil then
      behavior:setState('walk')
    end
    behavior:update(dt)
  end)

return {addAspect=addAspect, update=update}