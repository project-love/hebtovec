function love.conf(t)
  t.version = "0.10.1"
    
  t.window.title = "Hebtovec"    
  t.window.width = 512
  t.window.height = 512
  t.window.resizable = true
  t.window.minwidth = 128
  t.window.minheight = 128

  -- For debugging only. Disable before release.
  t.console = true
end
