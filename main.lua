local metaSystem
local entities

local function createDwarf(x, y, path)
  d = {position = {x=x, y=y},
       path = path}
     
  require('actor').addAspect(d)
  require('graphics.dwarf').addAspect(d)
  return d
end

function love.load(arg)
  if arg and arg[#arg] == "-debug" then require("mobdebug").start() end

  metaSystem = require('metaSystem').create('actor', 'graphics.dwarf', 'plane')

  -- All displays will be rendered to this virtual target
  canvas = love.graphics.newCanvas(128, 128)
  -- When scaling up, use nearest neighbor to give crisp results
  canvas:setFilter("linear", "nearest")
  
  entities = {
    createDwarf(5, 5, {{x=9, y=5}, {x=8, y=5}, {x=7, y=5}, {x=6, y=5}}),
    createDwarf(12, 10, {{x=6, y=10}, {x=7, y=10}, {x=8, y=10}, {x=9, y=10}, {x=10, y=10}, {x=11, y=10}}),
    {blocks={{12, 9}, {5, 4}}},
    }
  
end

function love.update(dt)
  metaSystem.update(entities, dt)
end

function love.draw()
  love.graphics.setCanvas(canvas)
  love.graphics.clear()
  metaSystem.draw(entities)
  love.graphics.setCanvas()
  
  -- remove any trasformation performed in the draw function
  love.graphics.reset()
  
  -- scale up to match window size
  local widthScale = love.graphics.getWidth() / canvas:getWidth() 
  local heightScale = love.graphics.getHeight() / canvas:getHeight() 
  local scale = math.min(widthScale, heightScale)
  love.graphics.translate(0, love.graphics.getHeight())
  love.graphics.scale(scale, -scale)
  
  -- draw canvas to window
  love.graphics.draw(canvas)
end

function love.keypressed(key, scancode, isrepeat)
  if key == "f" and not isrepeat then
    love.window.setFullscreen(not love.window.getFullscreen())
  end
end

