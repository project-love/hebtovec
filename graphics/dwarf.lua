local Behavior = require 'knife.behavior'
local System = require 'knife.system'
local Color = require 'graphics.db16'

local function head()
  love.graphics.setColor(Color.PEACH)
  love.graphics.polygon('fill', 3,3, -3,3, -3,-3, 3,-3) -- head
  love.graphics.setColor(Color.BROWN)
  love.graphics.polygon('fill', 3,3, -3,3, -3,-3, 1,-1) -- hair
  love.graphics.polygon('fill', 2,-1, 4,-2, 4,-6, 3,-6) --beard
end  
      
local function arm()
  love.graphics.setColor(Color.LIGHTGRAY)
  love.graphics.polygon('fill', 1,1, -1,1, -1,-3, 1,-3)
end

local function torso()
  love.graphics.setColor(Color.BLUEGRAY)
  love.graphics.polygon('fill', 4,-4, -4,-4, -4,3, 4,3)
end

local function frontLeg()
  love.graphics.setColor(Color.BLUEGRAY)
  love.graphics.polygon('fill', 1,1, -1,1, -1,-3, 1,-3)
end

local function backLeg()
  love.graphics.setColor(Color.DARKGRAY)
  love.graphics.polygon('fill', 1,1, -1,1, -1,-3, 1,-3)
end

local function drawPart(draw, xOffset, yOffset)
  love.graphics.push()
  love.graphics.translate(xOffset, yOffset)
  draw()
  love.graphics.pop()
end

local states = {
  default = {
    { duration = 0, after = 'idle' },
  },
  idle = {
    { duration = 1,
      {part=backLeg, translate={0,3}},
      {part=frontLeg, translate={0,3}},
      {part=torso, translate={0,7}},
      {part=head, translate={0,13}},
      {part=arm, translate={0,8}}}},
  walk = {
    { duration = 0.125,
      {part=backLeg, translate={1,3}},
      {part=frontLeg, translate={-1,3}},
      {part=torso, translate={0,6}},
      {part=head, translate={0,12}},
      {part=arm, translate={1,7}}},
    { duration = 0.125,
      {part=backLeg, translate={1,3}},
      {part=frontLeg, translate={1,4}},
      {part=torso, translate={1,7}},
      {part=head, translate={1,13}},
      {part=arm, translate={1,8}}},
    { duration = 0.125,
      {part=backLeg, translate={1,3}},
      {part=frontLeg, translate={3,3}},
      {part=torso, translate={2,6}},
      {part=head, translate={2,12}},
      {part=arm, translate={1,7}}},
    { duration = 0.125,
      {part=backLeg, translate={3,4}},
      {part=frontLeg, translate={3,3}},
      {part=torso, translate={3,7}},
      {part=head, translate={3,13}},
      {part=arm, translate={3,8}}},
    { duration = 0.125,
      {part=backLeg, translate={5,3}},
      {part=frontLeg, translate={3,3}},
      {part=torso, translate={4,6}},
      {part=head, translate={4,12}},
      {part=arm, translate={5,7}}},
    { duration = 0.125,
      {part=backLeg, translate={5,3}},
      {part=frontLeg, translate={5,4}},
      {part=torso, translate={5,7}},
      {part=head, translate={5,13}},
      {part=arm, translate={5,8}}},
    { duration = 0.125,
      {part=backLeg, translate={5,3}},
      {part=frontLeg, translate={7,3}},
      {part=torso, translate={6,6}},
      {part=head, translate={6,12}},
      {part=arm, translate={5,7}}},
    { duration = 0.125,
      {part=backLeg, translate={7,4}},
      {part=frontLeg, translate={7,3}},
      {part=torso, translate={7,7}},
      {part=head, translate={7,13}},
      {part=arm, translate={7,8}}}},
  
}

local function addAspect(entity)
  entity.animation = Behavior(states)
end

local update = System(
  { 'animation', 'path' },
  function (animation, path, dt)
    if animation.state == 'idle' and next(path) ~= nil then
      -- Should be moving!
      animation:setState('walk')
    elseif animation.state == 'walk' and next(path) == nil then
      animation:setState('idle')
    end
    
    animation:update(dt)
  end)
  
local draw = System(
  { 'animation', 'position', 'path' },
  function (animation, p, path)
    
    -- Find out which direction we are walking
    local nextPosition = path[#path]
    local direction
    if nextPosition == nil or nextPosition.x >= p.x then
      direction = 1
    else
      direction = -1
    end
    
    love.graphics.push()

      -- Draw it at the correct world offset
      -- Scale from world-space (1=block) to pixel-space (1=pixel)
      love.graphics.translate(p.x * 8, p.y * 8)
      
      -- Flip the graphics based on which direction it is pointing
      love.graphics.scale(direction, 1)

      -- Translate and draw each bone
      for i, bone in ipairs(animation.frame) do
        love.graphics.push()
          love.graphics.translate(bone.translate[1], bone.translate[2])
          bone.part()
        love.graphics.pop()
      end
      
    love.graphics.pop()
  end)

return {addAspect=addAspect, update=update, draw=draw}