-- DB16 - DawnBringer's 16 Col Palette v1.0
-- http://www.pixeljoint.com/forum/forum_posts.asp?TID=12795
return {
  BLACK = {20, 12, 28},
  DARKRED = {68, 36, 52},
  DARKBLUE = {48, 52, 109},
  DARKGRAY = {78, 74, 78},
  BROWN = {133, 76, 48},
  DARKGREEN = {52, 101, 36},
  RED = {208, 70, 72},
  LIGHTGRAY = {117, 113, 97},
  LIGHTBLUE = {89, 125, 206},
  ORANGE = {210, 125, 44},
  BLUEGRAY = {133, 149, 161},
  LIGHTGREEN = {109, 170, 44},
  PEACH = {210, 170, 153},
  CYAN = {109, 194, 202},
  YELLOW = {218, 212, 94},
  WHITE = {222, 238, 214}}