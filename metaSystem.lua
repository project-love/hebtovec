local Bind = require 'knife.bind'
local System = require 'knife.system'

local function create(...)
  local components = {}
  for i, componentName in ipairs{...} do
    components[i] = require(componentName)
  end
  
  local updateSystem = System(
      { 'update' },
      function (update, entities, dt)
        update(entities, dt)
      end)
    
  local drawSystem = System(
      { 'draw' },
      function (draw, entities)
        draw(entities)
      end)
  
  return {
    update = Bind(updateSystem, components),
    draw = Bind(drawSystem, components),
    }
end

return {create=create}